ARG FLUTTER_VERSION
FROM registry.gitlab.com/theonewiththebraid/flutter-dockerimages:$FLUTTER_VERSION-base

LABEL org.opencontainers.image.source=https://gitlab.com/TheOneWithTheBraid/flutter-dockerimages.git

ENV CMDLINE_TOOLS_VERSION=10406996

RUN apt-get update
# Flutter Android toolchain
RUN apt-get install -y --no-install-recommends openjdk-17-jdk libstdc++6 libpulse0 locales lcov
# Android x86_64 emulator toolchain
RUN bash -c 'if [[ $(uname -m) == "x86_64" ]]; then apt-get install -y --no-install-recommends libxtst6 libnss3-dev libnspr4 libxss1 libasound2 libatk-bridge2.0-0 libgtk-3-0 libgdk-pixbuf2.0-0; fi'
RUN rm -rf /var/cache/apt /var/lib/apt/lists/*

ENV ANDROID_HOME=/sdks/Android
ENV ANDROID_SDK_ROOT=${ANDROID_HOME}
ENV ANDROID_SDK_ROOT=${ANDROID_HOME}

ENV PATH ${PATH}:${ANDROID_HOME}/cmdline-tools/bin:${ANDROID_HOME}/platform-tools:${ANDROID_HOME}/tools:${ANDROID_HOME}/build-tools/34.0.0:${ANDROID_HOME}/build-tools/33.0.1:${ANDROID_HOME}/build-tools/30.0.3:${ANDROID_HOME}/build-tools/26.0.3

ADD sdk_components ${ANDROID_HOME}/sdk_components
ADD emulator_components ${ANDROID_HOME}/emulator_components

# Android SDK setup
RUN mkdir -p ${ANDROID_HOME}
WORKDIR ${ANDROID_HOME}
RUN curl "https://dl.google.com/android/repository/commandlinetools-linux-${CMDLINE_TOOLS_VERSION}_latest.zip" > /tmp/cmdlintools.zip
RUN unzip /tmp/cmdlintools.zip && rm /tmp/cmdlintools.zip
RUN yes | sdkmanager --sdk_root=${ANDROID_HOME} --licenses
RUN while read component; do yes | sdkmanager --sdk_root=${ANDROID_HOME} $component; done < ${ANDROID_HOME}/sdk_components
RUN bash -c 'if [[ $(uname -m) == "x86_64" ]]; then while read component; do yes | sdkmanager --sdk_root=${ANDROID_HOME} $component; done < ${ANDROID_HOME}/emulator_components ; fi'

WORKDIR $HOME
RUN flutter precache --android
