FROM debian:latest

LABEL org.opencontainers.image.source=https://gitlab.com/TheOneWithTheBraid/flutter-dockerimages.git

ARG FLUTTER_VERSION

ENTRYPOINT /bin/bash

ENV FLUTTER_GIT_URL=https://github.com/flutter/flutter.git
ENV FLUTTER_HOME=/sdks/flutter
ENV FLUTTER_ROOT=${FLUTTER_HOME}
ENV HOME=/home/flutter

RUN apt-get update
RUN apt-get dist-upgrade -y
# basic Flutter runtime dependencies
RUN apt-get install -y --no-install-recommends ca-certificates coreutils locales curl bash file git unzip xz-utils zip
# Flutter test runtime dependencies
RUN apt-get install -y --no-install-recommends libglu1-mesa

RUN apt-get autoremove
RUN rm -rf /var/cache/apt /var/lib/apt/lists/*

RUN sh -c 'echo "en_US.UTF-8 UTF-8" > /etc/locale.gen' && locale-gen && update-locale LANG=en_US.UTF-8

ENV PATH ${PATH}:${FLUTTER_HOME}/bin:${FLUTTER_HOME}/bin/cache/dart-sdk/bin:${HOME}/.pub-cache/bin

# Flutter SDK setup
RUN git clone --depth 1 --branch ${FLUTTER_VERSION} ${FLUTTER_GIT_URL} ${FLUTTER_HOME}
RUN cd ${FLUTTER_HOME} && git switch -c stable && flutter --suppress-analytics channel stable --no-cache-artifacts

ENV FLUTTER_GIT_URL="unknown source"

RUN flutter --suppress-analytics config --disable-analytics
RUN flutter doctor
RUN flutter precache --universal

WORKDIR $HOME
