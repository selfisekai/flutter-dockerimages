# Flutter Docker images

Functional multiarch images for Flutter CI/CD.

## Features

- supported architectures
    - aarch64 / arm64v8
    - x86_64 / amd64
- Clean build from Debian base
- Extensible via APT
- Includes Android SDK
- different flavors
    - basic: no platform specific build tools shipped, only for tests
    - web: only web toolchain shipped
    - linux: Linux desktop toolchain shipped
        - debian: Debian build tools shipped
    - android: Android toolchain shipped
        - shipped components:
            - [android/sdk_components](android/sdk_components) - all default SDK components
            - [android/emulator_components](android/emulator_components) - the components shipped on x86_64 emulator
              enabled devices

## Available tags

```shell
export FLUTTER_VERSION=3.16.5
# the base image without any specific toolchain, e.g. for tests
docker pull registry.gitlab.com/theonewiththebraid/flutter-dockerimages:$FLUTTER_VERSION-base
# Linux
docker pull registry.gitlab.com/theonewiththebraid/flutter-dockerimages:$FLUTTER_VERSION-linux
# Debian build image
docker pull registry.gitlab.com/theonewiththebraid/flutter-dockerimages-base:$FLUTTER_VERSION-debian
# web
docker pull registry.gitlab.com/theonewiththebraid/flutter-dockerimages-base:$FLUTTER_VERSION-web
# Android
docker pull registry.gitlab.com/theonewiththebraid/flutter-dockerimages-base:$FLUTTER_VERSION-android
```

All images have annotated manifests and will hence serve the right layers for `arm64v8` and `amd64` each.

### Channel images

*Do you support channel images, e.g. `stable` ?*

No. Every build should always pin to a particular Flutter version - as much as to particular dependency versions.
You will otherwise soon mess up your project.

## License

This packaged according to the terms and conditions of EUPL-1.2.
